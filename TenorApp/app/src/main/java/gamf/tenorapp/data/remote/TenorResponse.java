package gamf.tenorapp.data.remote;

import java.util.List;

public class TenorResponse {
    public List<TenorResponseItem> items;
    public double next;
}
