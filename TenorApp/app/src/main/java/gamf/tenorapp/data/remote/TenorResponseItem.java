package gamf.tenorapp.data.remote;

public class TenorResponseItem {
    public String id;
    public String title;
    public String previewUrl;
    public String gifUrl;
    public long size;
    public int shares;
    public String[] tags;
}
