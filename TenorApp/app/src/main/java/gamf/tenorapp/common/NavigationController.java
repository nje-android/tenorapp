package gamf.tenorapp.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import gamf.tenorapp.ui.MainActivity;
import gamf.tenorapp.ui.details.DetailsFragment;
import gamf.tenorapp.ui.search.SearchFragment;

public class NavigationController {
        private final int containerId;
        private final FragmentManager fragmentManager;

        public NavigationController(int containerId, MainActivity mainActivity) {
            this.containerId = containerId;
            this.fragmentManager = mainActivity.getSupportFragmentManager();
        }

        public void navigateToSearch() {
            loadFragment(SearchFragment.newInstance(), "search");
        }

        public void navigateToDetails() {
            loadFragment(DetailsFragment.newInstance(), "details");
        }

        private void loadFragment(Fragment fragment, String tag) {
            fragmentManager.beginTransaction()
                    .replace(containerId, fragment, tag)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }

        public void loadFragmentWithoutAddTotBackStack(Fragment fragment, String tag) {
            fragmentManager.beginTransaction()
                    .replace(containerId, fragment, tag)
                    .commitAllowingStateLoss();
        }
}
