package gamf.tenorapp.service;

import com.google.gson.Gson;

import gamf.tenorapp.data.remote.TenorResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TenorService {
    private final String BASE_URL = "https://api.tenor.com/v1/";
    private final Retrofit retrofit;

    public TenorService() {
        this.retrofit = buildRetrofit();
    }

    public void getTrending(Callback<TenorResponse> callback) {
        Call<TenorResponse> call = retrofit.create(TenorClient.class).getTrending();
        call.enqueue(callback);
    }

    public void searchByTag(String tag, Callback<TenorResponse> callback) {
        Call<TenorResponse> call = retrofit.create(TenorClient.class).searchByTag(tag);
        call.enqueue(callback);
    }

    private Retrofit buildRetrofit() {
        Gson gson = TenorDeserializer.createGsonBuilder()
                .create();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
