package gamf.tenorapp.service;

import gamf.tenorapp.data.remote.TenorResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TenorClient {
    @GET("trending")
    Call<TenorResponse> getTrending();

    @GET("search")
    Call<TenorResponse> searchByTag(@Query("tag") String tag);

    @GET("random?q=excited")
    Call<TenorResponse> getRandomExcited();
}
