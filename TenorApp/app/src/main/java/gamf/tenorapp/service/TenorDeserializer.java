package gamf.tenorapp.service;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import gamf.tenorapp.data.remote.TenorResponse;
import gamf.tenorapp.data.remote.TenorResponseItem;

public class TenorDeserializer {

    public static GsonBuilder createGsonBuilder() {
        JsonDeserializer<TenorResponse> deserializer = new JsonDeserializer<TenorResponse>() {
            @Override
            public TenorResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                JsonObject rootJsonObject = json.getAsJsonObject();
                JsonArray resultsArray = rootJsonObject.getAsJsonArray("results");
                List<TenorResponseItem> responseItems = new ArrayList<>();

                for (int i = 0; i < resultsArray.size(); i++) {
                    JsonObject item = resultsArray.get(i).getAsJsonObject();
                    TenorResponseItem responseItem = new TenorResponseItem();
                    responseItem.id = item.get("id").getAsString();
                    responseItem.title = item.get("title").getAsString();
                    responseItem.shares = item.get("shares").getAsInt();

                    JsonArray tagsArray = item.getAsJsonArray("tags");
                    responseItem.tags = new String[tagsArray.size()];
                    for (int k = 0; k < tagsArray.size(); k++) {
                        responseItem.tags[k] = tagsArray.get(k).getAsString();
                    }

                    JsonObject gifObject = item.getAsJsonArray("media")
                            .get(0)
                            .getAsJsonObject().get("gif")
                            .getAsJsonObject();

                    responseItem.previewUrl = gifObject.get("preview").getAsString();
                    responseItem.gifUrl = gifObject.get("url").getAsString();
                    responseItem.size = gifObject.get("size").getAsLong();

                    responseItems.add(responseItem);
                }

                TenorResponse response = new TenorResponse();
                response.items = responseItems;
                response.next = rootJsonObject.get("next").getAsDouble();

                return response;
            }
        };

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(TenorResponse.class, deserializer);

        return gsonBuilder;
    }
}
