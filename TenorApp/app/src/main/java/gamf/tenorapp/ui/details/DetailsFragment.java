package gamf.tenorapp.ui.details;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import gamf.tenorapp.R;
import gamf.tenorapp.common.GlideApp;
import gamf.tenorapp.ui.MainViewModel;

public class DetailsFragment extends Fragment {

    private MainViewModel viewModel;

    public DetailsFragment() {
    }

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        final TextView titleTextView = rootView.findViewById(R.id.TitleTextView);
        final ImageView gifImageView = rootView.findViewById(R.id.GifImageView);
        final TextView tagsTextView = rootView.findViewById(R.id.TagsTextView);

        viewModel.getSelectedItem().observe(this, value -> {
            if (value == null) {
                Toast.makeText(getActivity(), "Nincs megjeleníthető adat", Toast.LENGTH_SHORT).show();
                return;
            }

            titleTextView.setText(value.getTitle());
            tagsTextView.setText(value.getTags());

            GlideApp.with(getActivity())
                    .load(value.getGifUrl())
                    .into(gifImageView);
        });

        return rootView;
    }
}
