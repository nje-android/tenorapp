package gamf.tenorapp.ui.search;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gamf.tenorapp.R;
import gamf.tenorapp.ui.MainActivity;
import gamf.tenorapp.ui.MainViewModel;

public class SearchFragment extends Fragment {

    private MainViewModel viewModel;
    private SearchAdapter searchAdapter;
    private List<SearchItem> searchItems;

    private EditText SearchEditText;
    private RecyclerView SearchResultRecyclerView;
    private ProgressBar LoadingProgressBar;

    public SearchFragment() {
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        searchItems = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        SearchEditText = rootView.findViewById(R.id.SearchEditText);
        LoadingProgressBar = rootView.findViewById(R.id.LoadingProgressBar);
        initRecyclerView(rootView);
        initSearchButton(rootView);

        viewModel.getSearchItems().observe(this, value -> {
            showList();
            if (value == null) {
                Toast.makeText(getActivity(), "Szolgáltatás elérése sikertelen", Toast.LENGTH_SHORT).show();
                return;
            }

            searchItems.clear();
            searchItems.addAll(value);
            searchAdapter.notifyDataSetChanged();
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        String input = SearchEditText.getText().toString();
        if (input.isEmpty() || input.length() < 3) {
            showProgressBar();
            viewModel.getTrending();
        }
    }

    public void selectItemAndNavigate(SearchItem item) {
        viewModel.selectItem(item);
        ((MainActivity) getActivity()).navigateToDetails();
    }

    private void initRecyclerView(View rootView) {
        SearchResultRecyclerView = rootView.findViewById(R.id.SearchResultRecyclerView);
        // adapter
        searchAdapter = new SearchAdapter(this, searchItems);
        // layout manager
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(getContext());

        SearchResultRecyclerView.setLayoutManager(layoutManager);
        SearchResultRecyclerView.setAdapter(searchAdapter);
    }

    private void initSearchButton(View rootView) {
        Button searchButton = rootView.findViewById(R.id.SearchButton);

        searchButton.setOnClickListener(v -> {
            String input = SearchEditText.getText().toString();

            if (input.isEmpty() || input.length() < 3) {
                Toast.makeText(getActivity(), "A kereséshez minimum három karaktert írjon be", Toast.LENGTH_SHORT).show();
                return;
            }

            showProgressBar();
            viewModel.search(input);
        });
    }

    private void showProgressBar() {
        SearchResultRecyclerView.setVisibility(View.GONE);
        LoadingProgressBar.setVisibility(View.VISIBLE);
    }

    private void showList() {
        LoadingProgressBar.setVisibility(View.GONE);
        SearchResultRecyclerView.setVisibility(View.VISIBLE);
    }
}
