package gamf.tenorapp.ui.search;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import gamf.tenorapp.R;
import gamf.tenorapp.common.GlideApp;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchItemHolder> {

    private final SearchFragment searchFragment;
    private List<SearchItem> searchItems;

    public SearchAdapter(SearchFragment searchFragment, List<SearchItem> searchItems) {
        this.searchFragment = searchFragment;
        this.searchItems = searchItems;
    }

    @NonNull
    @Override
    public SearchItemHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_result_list_item,
                        parent, false);
        return new SearchItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchItemHolder holder,
                                 int position) {
        final SearchItem searchItem = searchItems.get(position);

        // képi erőforrás megjelenítése
        GlideApp.with(searchFragment)
                .load(searchItem.getPreviewUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Log.e("SearchAdapter", e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Log.v("SearchAdapter", "Letöltés sikeres: " + searchItem.getPreviewUrl());
                        return false;
                    }
                })
                .into(holder.previewImageView);

        // ImageView kattintás: kiválasztott elem továbbadása és navigáció
        holder.previewImageView.setOnClickListener(v -> {
            searchFragment.selectItemAndNavigate(searchItem);
        });
    }

    @Override
    public int getItemCount() {
        return searchItems.size();
    }

    public class SearchItemHolder
            extends RecyclerView.ViewHolder {
        public ImageView previewImageView;

        public SearchItemHolder(View itemView) {
            super(itemView);
            previewImageView = itemView.findViewById(R.id.PreviewImageView);
        }
    }
}

