package gamf.tenorapp.ui.search;

public class SearchItem {

    private String previewUrl;
    private String title;
    private String gifUrl;
    private String tags;

    public SearchItem(String previewUrl, String title, String gifUrl, String tags) {
        this.previewUrl = previewUrl;
        this.title = title;
        this.gifUrl = gifUrl;
        this.tags = tags;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getGifUrl() {
        return gifUrl;
    }

    public String getTags() {
        return tags;
    }
}
