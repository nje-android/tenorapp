package gamf.tenorapp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import gamf.tenorapp.R;
import gamf.tenorapp.common.NavigationController;
import gamf.tenorapp.service.TenorService;

public class MainActivity extends AppCompatActivity {

    private NavigationController navigationController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationController = new NavigationController(R.id.ContainerFrameLayout, this);
        navigationController.navigateToSearch();
    }

    public void navigateToDetails() {
        navigationController.navigateToDetails();
    }
}
