package gamf.tenorapp.ui.search;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import gamf.tenorapp.data.remote.TenorResponse;
import gamf.tenorapp.data.remote.TenorResponseItem;

public class TenorResponseMapper {
    public List<SearchItem> mapToSearchItems(TenorResponse response) {
        List<SearchItem> searchItems = new ArrayList<>();
        for (TenorResponseItem item : response.items) {
            String tags = TextUtils.join(", ", item.tags);
            searchItems.add(
                    new SearchItem(item.previewUrl, item.title, item.gifUrl, tags));
        }

        return searchItems;
    }
}
