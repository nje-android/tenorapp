package gamf.tenorapp.ui;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.List;

import gamf.tenorapp.data.remote.TenorResponse;
import gamf.tenorapp.service.TenorService;
import gamf.tenorapp.ui.search.SearchItem;
import gamf.tenorapp.ui.search.TenorResponseMapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {

    private TenorResponseMapper mapper;
    private TenorService service;
    private MutableLiveData<List<SearchItem>> searchItems;
    private MutableLiveData<SearchItem> selectedItem;

    public MainViewModel() {
        mapper = new TenorResponseMapper();
        service = new TenorService();
        searchItems = new MutableLiveData<>();
        selectedItem = new MutableLiveData<>();
    }

    public void getTrending() {
        service.getTrending(new Callback<TenorResponse>() {
            @Override
            public void onResponse(Call<TenorResponse> call, Response<TenorResponse> response) {
                searchItems.setValue(mapper.mapToSearchItems(response.body()));
            }

            @Override
            public void onFailure(Call<TenorResponse> call, Throwable t) {
                Log.e("MainViewModel", t.getMessage());
                searchItems.setValue(null);
            }
        });
    }

    public void search(String tag) {
        service.searchByTag(tag, new Callback<TenorResponse>() {
            @Override
            public void onResponse(Call<TenorResponse> call, Response<TenorResponse> response) {
                searchItems.setValue(mapper.mapToSearchItems(response.body()));
            }

            @Override
            public void onFailure(Call<TenorResponse> call, Throwable t) {
                Log.e("MainViewModel", t.getMessage());
                searchItems.setValue(null);
            }
        });
    }

    public MutableLiveData<List<SearchItem>> getSearchItems() {
        return searchItems;
    }

    public MutableLiveData<SearchItem> getSelectedItem() {
        return selectedItem;
    }

    public void selectItem(SearchItem item) {
        selectedItem.setValue(item);
    }
}
